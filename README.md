# ExpressO

ExpressO (Mobile App), Plantation (API services) and Barista (Admin Interface) ordering service

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

[Docker](https://store.docker.com/) - Containerization

[Node](https://nodejs.org/) - Node JS Runtime Version 10.1.0

### Tooling

Basic tooling to get a runnign development enviroment

[Visual Studio Code](https://code.visualstudio.com/) - IDE

[DBeaver](https://dbeaver.io/) - Database client

[Sourcetree](https://www.sourcetreeapp.com/) - Git Source Control Client

## Run the application in Development

## Plantation Database

A Posgres database is containerized using docker. start the application using the following command in the *root project directory*

If using powershell on windows run this command first

```
$env:COMPOSE_CONVERT_WINDOWS_PATHS=1
```

```
docker-compose up
```

you can then inspect the database using **DBeaver**


## Plantation Services

from the *root project directory* navigate to the *./plantation* folder

```
cd ./plantation
```

begin typescript compilation for development 

```
npm run watch:tsc
```

## Barista

from the *root project directory* navigate to the *./barista* folder

```
cd ./barista
```

then start the development server with the npm command

```
npm run start
```

## Additional Infomartion

Please check individual **README.md** files for additional information in each sub-project

## Running the tests

**TODO**: 
Explain how to run the automated tests for this system

### Break down into end to end tests

**TODO**:

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Coding style is enforced by tslint

```
Give an example
```

## Deployment

**TODO**: 

## Built With

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Aadil Cachalia** - *Initial work* - [CodeAssembly](https://codeassembly.co.za)


## License

**TODO**: This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc

