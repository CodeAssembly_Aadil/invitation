'use strict';

exports.up = function up(knex, Promise) {
	return knex.schema.raw(
		`
		CREATE EXTENSION IF NOT EXISTS "postgis";
		CREATE EXTENSION IF NOT EXISTS "postgis_topology";
		CREATE EXTENSION IF NOT EXISTS "fuzzystrmatch";
		CREATE EXTENSION IF NOT EXISTS "postgis_tiger_geocoder";
		CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
		CREATE EXTENSION IF NOT EXISTS "pg_trgm";
		`
	);
};

exports.down = function down(knex, Promise) {
	return knex.schema.raw(
		`
		--DROP EXTENSION IF EXISTS "postgis";
		--DROP EXTENSION IF EXISTS "postgis_topology";
		--DROP EXTENSION IF EXISTS "fuzzystrmatch";
		--DROP EXTENSION IF EXISTS "postgis_tiger_geocoder";
		--DROP EXTENSION IF EXISTS "uuid-ossp";
		--DROP EXTENSION IF EXISTS "pg_trgm";
		`
	);
};
