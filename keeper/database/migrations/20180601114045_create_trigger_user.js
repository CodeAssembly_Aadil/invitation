'use strict';

const { USER_TABLE } = require('../tables');
const { SET_UPDATED_AT_TIMESTAMP } = require('../fucns');

const TRIGGER_NAME = 'set_updated_at_timestamp_user';

exports.up = function (knex, Promise) {
	return knex.schema.raw(`
		CREATE TRIGGER ${TRIGGER_NAME}
		BEFORE UPDATE 
		ON "${USER_TABLE}"
		FOR EACH ROW
		EXECUTE PROCEDURE ${SET_UPDATED_AT_TIMESTAMP}();
	`);
};

exports.down = function (knex, Promise) {
	return knex.schema.raw(`DROP TRIGGER IF EXISTS ${TRIGGER_NAME} ON "${USER_TABLE}";`);
};
