'use strict';

const { USER_TABLE, TOKEN_TABLE } = require('../tables');


exports.up = function (knex, Promise) {
	return knex.schema.table(TOKEN_TABLE,
		table => {
			table.foreign('user').references(`${USER_TABLE}.id`).onDelete('CASCADE');
		})
};

exports.down = function (knex, Promise) {
	return knex.schema.table(TOKEN_TABLE,
		table => {
			table.dropForeign('user');
		});
};
