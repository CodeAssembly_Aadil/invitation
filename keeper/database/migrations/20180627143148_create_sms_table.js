'use strict';

const { SMS_TABLE } = require('../tables');

exports.up = function (knex, Promise) {

	return knex.schema.hasTable(SMS_TABLE).then(
		exists => {
			if (!exists) {
				return knex.schema.createTable(SMS_TABLE,
					table => {

						table.bigIncrements('id').primary();

						table.uuid('clientMessageId').unique();

						table.string('to', 64);
						table.string('from', 64);

						table.string('content', 160);

						table.timestamp('scheduledDeliveryTime', 'UTC');
						table.integer('validityPeriod').unsigned().defaultTo(1440);

						table.string('userDataHeader', 32);
						table.string('binary').nullable();

						table.string('charset', 16).defaultTo('UTF-8');

						table.timestamp('createdAt', 'UTC').defaultTo(knex.fn.now());
					});
			}
		});
};

exports.down = function (knex, Promise) {
	return knex.schema.dropTableIfExists(SMS_TABLE);
};