'use strict';

const { SMS_STATUS_TABLE } = require('../tables');

exports.up = function (knex, Promise) {

	return knex.schema.hasTable(SMS_STATUS_TABLE).then(
		exists => {
			if (!exists) {
				return knex.schema.createTable(SMS_STATUS_TABLE,
					table => {

						table.bigIncrements('id').primary();

						table.string('integrationName', 255);
						table.string('messageId', 64);
						table.string('requestId', 64);

						table.uuid('clientMessageId');

						table.integer('statusCode');
						table.string('status', 64);
						table.string('statusDescription', 255);

						table.timestamp('timestamp', 'UTC');

						table.timestamp('createdAt', 'UTC').defaultTo(knex.fn.now());
					});
			}
		});
};

exports.down = function (knex, Promise) {
	return knex.schema.dropTableIfExists(SMS_STATUS_TABLE);
};