'use strict';

const { SMS_TABLE, SMS_STATUS_TABLE } = require('../tables');


exports.up = function (knex, Promise) {
	return knex.schema.table(SMS_STATUS_TABLE,
		table => {
			table.foreign('clientMessageId').references(`${SMS_TABLE}.clientMessageId`).onDelete('CASCADE');
		})
};

exports.down = function (knex, Promise) {
	return knex.schema.table(SMS_STATUS_TABLE,
		table => {
			table.dropForeign('clientMessageId');
		});
};
