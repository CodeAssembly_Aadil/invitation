'use strict';

const { SMS_USER_TABLE,SMS_TABLE, USER_TABLE } = require('../tables');

exports.up = function (knex, Promise) {

	return knex.schema.hasTable(SMS_USER_TABLE).then(
		exists => {
			if (!exists) {
				return knex.schema.createTable(SMS_USER_TABLE,
					table => {

						table.bigIncrements('id').primary();

						table.uuid('clientMessageId').references(`${SMS_TABLE}.clientMessageId`).onDelete('CASCADE');
						table.uuid('user').references(`${USER_TABLE}.id`).onDelete('CASCADE');

						table.timestamp('createdAt', 'UTC').defaultTo(knex.fn.now());
					});
			}
		});
};

exports.down = function (knex, Promise) {
	return knex.schema.dropTableIfExists(SMS_USER_TABLE);
};