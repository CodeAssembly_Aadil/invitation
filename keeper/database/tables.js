'use strict'

module.exports = {
	USER_TABLE: 'User',
	TOKEN_TABLE: 'Token',
	OTP_TABLE: 'OTP',
	SMS_TABLE: 'SMS',
	SMS_STATUS_TABLE: 'SMSStatus',
	SMS_USER_TABLE: 'SMSUser',
};
