import * as Boom from 'boom';

import { isPlainObject } from 'lodash';

import {
	AuthenticationData,
	Lifecycle,
	Request,
	ResponseToolkit,
	Server,
	ServerAuthSchemeObject,
} from 'hapi';

// Internals

const AUTH_BEARER_TOKEN = 'auth-bearer-token';

// Implementation

export default function AuthBearerTokenScheme(server: Server, options?: any): ServerAuthSchemeObject {
	return {
		async authenticate(request: Request, h: ResponseToolkit): Promise<Lifecycle.ReturnValue | void> {

			const authorizationHeader: string = request.raw.req.headers.authorization;

			if (!authorizationHeader) {
				throw Boom.unauthorized('Missing authorization', AUTH_BEARER_TOKEN);
			}

			const parts: string[] = authorizationHeader.match(/^Bearer ([0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}); Token ([0-9a-z]{64})$/i);

			if (!parts) {
				throw Boom.unauthorized('Invalid authorization', AUTH_BEARER_TOKEN);
			}

			const bearerID = parts[1];
			const token = parts[2];

			const { isValid, credentials, response } = await options.validate(request, bearerID, token, h);

			if (response !== undefined) {
				return h.response(response).takeover();
			}

			const data: AuthenticationData = { credentials };

			if (!isValid) {
				return h.unauthenticated(Boom.unauthorized('Bad authorization token', AUTH_BEARER_TOKEN), data);
			}

			if (!credentials || !isPlainObject(credentials)) {
				throw Boom.badImplementation('Bad credentials');
			}

			return h.authenticated(data);
		},

	};
}
