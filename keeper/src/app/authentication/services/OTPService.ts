import * as Crypto from 'crypto';
import * as Utils from 'util';

import * as Joi from 'joi';

import OTP from 'app/authentication/models/OTP';
import AuthRepository from 'app/authentication/repository/AuthRepository';
import User from 'app/users/models/User';
import UserRepository from 'app/users/repositories/UserRepository';

const randomBytes = Utils.promisify(Crypto.randomBytes);

export default class OTPService {
	static async generateOTP(): Promise<string> {
		const otpBuffer: Buffer = await randomBytes(16);

		// 8 digit otp
		const otp = parseInt(otpBuffer.toString('hex'), 16).toString().substr(0, 8);

		return otp;
	}

	static async validateOTPForUser(phoneNumber: string, otp: string): Promise<{ user: User, otpIsValid: boolean }> {

		const user: User = await UserRepository.findUserByPhone(phoneNumber);

		if (!user) {
			return { user, otpIsValid: false };
		}

		const userOTP: OTP = await AuthRepository.findOTPForUser(user.uuid);

		if (!userOTP) {
			return { user, otpIsValid: false };
		}

		Joi.object().keys({
			otp: Joi.string().equal(otp),
			expiresAt: Joi.date().max('now')

		});

		return { user, otpIsValid: false };
	}

}
