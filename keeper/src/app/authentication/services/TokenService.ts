import * as Crypto from 'crypto';
import * as Utils from 'util';

const randomBytes = Utils.promisify(Crypto.randomBytes);

export default class TokenService {
	static async generateAPToken(): Promise<string> {
		const tokenBuffer: Buffer = await randomBytes(48);

		// 64 digit token
		const token = tokenBuffer.toString('base64').toString().substr(0, 64);

		return token;
	}
}
