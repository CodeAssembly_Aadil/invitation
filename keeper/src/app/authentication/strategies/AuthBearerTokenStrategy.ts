import { Request, ResponseToolkit, Server } from 'hapi';

import { pick } from 'lodash';

import AuthBearerTokenScheme from 'app/authentication/schemes/AuthBearerTokenScheme';
import User from 'app/users/models/User';
import UserRepository from 'app/users/repositories/UserRepository';

const AUTH_BEARER_TOKEN: string = 'auth-bearer-token';

async function validateAuthBearerToken(request: Request, bearerID: string, token: string, h: ResponseToolkit) {

	const user: User = await UserRepository.findUserWithToken(bearerID, token);

	let isValid = false;
	let credentials = null;

	if (Boolean(user)) {
		// User exists in DB user is activated and user is not locked out
		isValid = Boolean(user.activatedAt) && !Boolean(user.lockedAt);
		credentials = pick(user, ['uuid', 'firstName', 'lastName', 'phoneNumber', 'createdAt', 'updatedAt']);
	}

	return { isValid, credentials };
}

export default function AuthBearerTokenStrategy(schemeID: string, server: Server) {
	server.auth.scheme(schemeID, AuthBearerTokenScheme);
	server.auth.strategy(AUTH_BEARER_TOKEN, schemeID, {
		validate: validateAuthBearerToken,
	});
}
