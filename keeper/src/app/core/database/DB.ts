import * as Knex from 'knex';
import * as KnexPostgis from 'knex-postgis';

import { Database as DatabaseConfig } from 'config/Database';

const DB = Knex(DatabaseConfig);
// Add postgis extention
KnexPostgis(DB);

export default DB;
