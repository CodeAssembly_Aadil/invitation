import * as Joi from 'joi';

export const PhoneNumberSchema: Joi.Schema = Joi.string()
	.regex(/^0[0-9]{9}$/)
	.required()
	.label('Phone Number');
