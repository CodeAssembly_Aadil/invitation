import Joi from 'joi';

export const UUIDV4Schema: Joi.Schema = Joi.string().uuid({ version: 'uuidv4' });
