/**
 * Clickatell SMS Message format
 *
 * https://www.clickatell.com/developers/api-documentation/rest-api-request-parameters/
 */
export interface SMS {
	/** Unique client message identifier (UUID) */
	clientMessageId: string;
	/** Numbers to deliver the message to (number must start with '+') */
	to: string[];
	/** The two-way number that will be used for message delivery */
	from?: string;
	/** Content of the message (67 - 160 characters max length) encoding and content dependant */
	content: string;
	/** Our system will attempt to resubmit unsuccessful messages for the duration of the validity period.
	 * The request will be cancelled if it has not been successfully processed once the validity period is
	 *  exceeded. The default period is 1440 minutes (1 day).
	 */
	validityPeriod: number;
	/** Message body (content) delivered by client needs to be in binary format */
	binary?: any;
	/** Schedule your message to be delivered at a specified time in format: yyyy-MM-ddTHH:mm:ss+0200 (with
	 *  +0200 representing the time zone). Example: “scheduledDeliveryTime":"2017-02-27T14:30:00+0200"
	 */
	scheduledDeliveryTime?: Date;
	/** UDH (user data header) content of a message Base 16 Hexadecimal value, needed for multipart messages */
	userDataHeader?: string;
	/** ASCII, UCS2-BE, UTF-8, Windows-1252. If you do not specify the charset, it will be treated as UTF-8 by default. */
	charset?: string;
}
