/**
 * Clickatell SMS Message format
 *
 * https://www.clickatell.com/developers/api-documentation/rest-api-reply-callback/
 */
export interface SMSReply {
	/** Name of integration used for delivery */
	integrationName: string;
	/** ID of original message to which reply was sent */
	messageId: string;
	/** ID of the reply message */
	replyMessageId: string;
	/** The number from which we received the reply */
	fromNumber: string;
	/** The number to which reply was sent */
	toNumber: string;
	/** Timestamp of last message's status */
	timestamp: Date;
	/** Content of the reply message */
	text: string;
	/** Content of the reply message */
	udh: string;
	/** UDH (User data header) content of the message */
	charset: string;
	/** Network ID */
	network: string;
	/** Keyword */
	keyword: string;
}
