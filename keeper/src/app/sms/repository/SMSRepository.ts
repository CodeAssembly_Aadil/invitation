import DB from 'app/core/database/DB';

import { SMS_STATUS_TABLE, SMS_TABLE, USER_TABLE, SMS_USER_TABLE } from 'app/core/database/Tables';
import { SMS } from 'app/sms/models/SMS';
import { SMSStatus } from 'app/sms/models/SMSStatus';

export default class SMSRepository {

	static async saveSMS(sms: SMS): Promise<SMS> {
		return await DB(SMS_TABLE)
			.insert(sms, ['*']);
	}

	static async saveSMSStatus(smsStatus: SMSStatus): Promise<SMSStatus> {
		return await DB(SMS_STATUS_TABLE)
			.insert(smsStatus, ['*']);
	}
}
