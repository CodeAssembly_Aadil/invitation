
import { DateTime, Zone } from 'luxon';

export default class SMSDataMapper {
	/**
	 *  Clickatell date scheduledDeliveryTimeDate string
	 * @param date Javascript Date
	 * @returns string with +0200 hours offset
	 */
	static mapScheduledDeliveryTimeDateToString(date: Date): string {
		return DateTime.fromJSDate(date).setZone('UTC+2').toFormat(`yyyy-LL-dd'T'HH:mm:ssZZZ`);
	}

	static mapScheduledDeliveryTimeStringToDate(dateString: string): Date {
		return DateTime.fromISO(dateString).toJSDate();
	}
}