import { Plugin, PluginBase, PluginNameVersion, Request, ResponseToolkit, Server, ServerAuthScheme } from 'hapi';

import { HTTPMethod } from 'app/lib/http/HTTPMethods';
import { RegisterUserSchema } from 'app/users/schemas/RegisterUser';
import UserService from 'app/users/services/UserServices';

const UserPlugin: Plugin<PluginNameVersion> = {

	name: 'Keeper.Users',
	version: '1.0.0',
	multiple: false,
	dependencies: undefined,
	once: true,

	async register(server: Server, options: any): Promise<void> {

		server.route({
			method: HTTPMethod.POST,
			path: '/register-user',
			handler: UserService.registerUser,
			options: {
				security: true,
				validate: {
					payload: RegisterUserSchema,
				},
			},
		});
	},
};

module.exports = UserPlugin;
