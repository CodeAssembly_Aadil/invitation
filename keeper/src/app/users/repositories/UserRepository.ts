import { union } from 'lodash';

import DB from 'app/core/database/DB';
import { OTP_TABLE, TOKEN_TABLE, USER_TABLE } from 'app/core/database/Tables';
import User from 'app/users/models/User';

export default class UserRepository {

	static async saveUser(user: User): Promise<User> {
		return await DB(USER_TABLE)
			.insert(user, ['*']);
	}

	static async findUserByPhoneNumberWithOTP(phoneNumber: string, otp: string): Promise<User> {

		const user = await DB(USER_TABLE)
			.where('phoneNumber', '=', phoneNumber)
			.innerJoin(OTP_TABLE, `${USER_TABLE}.id`, `${OTP_TABLE}.user`)
			.where('otp', '=', otp)
			.where('expiresAt', '<', DB.fn.now())
			.first('*');

		return user as User;
	}

	static async findUserWithToken(id: string, token: string): Promise<User> {
		const user = await DB(USER_TABLE)
			.where('id', '=', id)
			.innerJoin(TOKEN_TABLE, `${USER_TABLE}.id`, `${TOKEN_TABLE}.user`)
			.where('token', '=', token)
			.where('expiresAt', '<', DB.fn.now())
			.first('*');

		return user as User;
	}

	static async findUser(id: string, fields: string[] = null): Promise<User> {

		const columns: string[] = fields ? union(['id'], fields) : ['*'];

		const user = await DB(USER_TABLE)
			.where('id', '=', id)
			.first(columns);

		return user as User;
	}

	static async findUserByPhone(phoneNumber: string, fields: string[] = null): Promise<User> {

		const columns: string[] = fields ? union(['phoneNumber'], fields) : ['*'];

		const user = await DB(USER_TABLE)
			.where('phoneNumber', '=', phoneNumber)
			.first(columns);

		return user as User;
	}

	static async findUsersByPhoneNumber(phoneNumbers: string[], fields: string[] = null): Promise<User[]> {
		const columns: string[] = fields ? union(['phoneNumber'], fields) : ['*'];

		return = await DB(USER_TABLE)
			.select(columns)
			.whereIn('phoneNumber', phoneNumbers);
	}
}
