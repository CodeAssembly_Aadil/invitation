import { Request, ResponseToolkit } from 'hapi';

import * as Boom from 'boom';

import { DateTime } from 'luxon';

import User from 'app/users/models/User';
import UserRepository from 'app/users/repositories/UserRepository';

export default class UserService {
	static async registerUser(request: Request, h: ResponseToolkit) {
		const { firstName, lastName, phoneNumber, acceptedTerms } = request.payload as any;

		const userData: User = { firstName, lastName, phoneNumber, acceptedTermsAt: acceptedTerms ? DateTime.utc().toJSDate() : null };

		let user: User;

		try {
			user = await UserRepository.saveUser(userData);
		} catch (error) {
			if (error.message.includes('duplicate key value violates unique constraint')) {
				return Boom.badData('Account is already registered');
			}

			return Boom.badImplementation('UserController.store', error);
		}

		return user;
	}
}
