import { RouteOptionsSecureObject } from 'hapi';

const RouteSecurityPolicy: RouteOptionsSecureObject = {
	hsts: true,
	xframe: 'deny',
	xss: true,
	noOpen: true,
	noSniff: true,
};

export default RouteSecurityPolicy;
