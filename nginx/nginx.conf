
events {
    # configuration of connection processing
}

http {
	server {
		listen 80 default_server;
		listen [::]:80 default_server;
		server_name www.plantation.local plantation.local;
		access_log off;
		error_log off;
		return 301 https://$host$request_uri;
	}

	# server {
	# 	listen 9229 default_server;
	# 	listen [::]:9229 default_server;
	# 	server_name www.plantation.local plantation.local;
	# 	access_log off;
	# 	error_log off;
	# 	location / {
	# 		proxy_pass http://10.0.0.3:9229;
	# 	}
	# }

	server {
		## start ssl config ##
		listen 443 ssl http2;
		server_name www.plantation.local plantation.local;

		## redirect www to non-www
		if ($host = 'www.plantation.local' ) {
			rewrite  ^/(.*)$  https://plantation.local/$1  permanent;
		}

		### SSL Config
		ssl_certificate /etc/nginx/certs/plantation.local.crt;
		ssl_certificate_key /etc/nginx/certs/plantation.local.key;
		ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
		keepalive_timeout 70;
		# Connection credentials caching
		ssl_session_cache shared:SSL:20m;
		ssl_session_timeout 180m;
		# Optimizing the cipher suites
		ssl_prefer_server_ciphers on;
		ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DHE+AES128:!ADH:!AECDH:!MD5;
		# DH parameters
		ssl_dhparam /etc/nginx/certs/plantation.local.dhparam.pem;
		
		# OCSP stapling
		# ssl_stapling on;
		# ssl_stapling_verify on;
		# ssl_trusted_certificate /etc/nginx/cert/trustchain.crt;
		# resolver 8.8.8.8 8.8.4.4;

		# Strict Transport Security
		add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

		## PROXY backend 
		location / {
			add_header Front-End-Https on;
			add_header Cache-Control "public, must-revalidate";
			add_header Strict-Transport-Security "max-age=2592000; includeSubdomains";
			add_header Referrer-Policy "same-origin";
			proxy_pass http://10.0.0.3:4430;
			proxy_next_upstream error timeout invalid_header http_500 http_502 http_503;
			proxy_set_header Host $host;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		}
	}
}